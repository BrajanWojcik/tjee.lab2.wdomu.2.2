/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Trwalosc;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author student
 */
public class LudzieQuery {
    
    private Session session = null;
    private List<Crosswords> ludzieList = null;
    private Query q = null;
    
    public String getLudzieLista(boolean OrderByImie) {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            if (OrderByImie) {
                q = session.createQuery("from Crosswords order by Name");
            } else {
                q = session.createQuery("from Crosswords");
            }
            ludzieList = (List<Crosswords>) q.list();
            session.close();
            tx.commit();
        } catch (HibernateException e) {
        }
        //return ludzieList;
        String data = getListaHTML(ludzieList);
        return data;
    }
    
    private String getListaHTML(List<Crosswords> lista) {
        String wiersz;
        wiersz = ("<table><tr>");
        wiersz = wiersz.concat(
            "<td><b>Crossword_id</b></td>"
            + "<td><b>Name</b></td>"
            + "<td><b>Price_PLN</b></td>"
            + "<td><b>Pages</b></td>"
            + "<td><b>Format</b></td>"
            + "<td><b>Editor_name</b></td>"
            + "<td><b>Has_other_puzzles</b></td>");
        wiersz = wiersz.concat("</tr>");
        for (Crosswords ldz : lista) {
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<td>" + ldz.getCrosswordId() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getName() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getPricePln() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getPages() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getFormat() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getEditorName() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getHasOtherPuzzles() + "</td>");
            wiersz = wiersz.concat("</tr>");
        }
        wiersz = wiersz.concat("</table>");
        return wiersz;
    }
    
    public void Usuniecie(String id) {
        try {
            /*org.hibernate.Transaction tx = session.beginTransaction();
            Crosswords crosswords = (Crosswords) session.get(Crosswords.class, id);
            crosswords.setName(im);
            session.update(crosswords);
            tx.commit();
            
            /*org.hibernate.Transaction tx = session.beginTransaction();
            //q = session.createQuery("insert into Crosswords (crosswordId, name) values (" + id + ", " + im + ")");
            //q = session.createQuery("insert into Crosswords (crosswordId, name, pricePln, pages, format, editorName, hasOtherPuzzles) values (2, 'Prosta', 13, 200, 'A3', 'Michal Bar', 'null')");
            //q = session.createQuery("delete Crosswords where crosswordId = " + id + " ");
            //Query q = session.createQuery("update Crosswords s set s.name =: " + im + " where s.crosswordId =: " + id + " ");
            q.executeUpdate();
            session.close();
            tx.commit();*/
            
            org.hibernate.Transaction tx = session.beginTransaction();
            q = session.createQuery("delete Crosswords where crosswordId = " + id + " ");
            q.executeUpdate();
            session.close();
            tx.commit();
        }
        catch (HibernateException e)
        {}
        /*org.hibernate.Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Crosswords");
        query.setParameter(1,id);
        query.setParameter(2,im);
        query.executeUpdate();*/
        
        
    }
    
    public LudzieQuery() {
        this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
    }
}
